const express = require('express');
const app = express();
const bodyBarser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');
const helmet = require('helmet');


mongoose
    .connect('mongodb://ezzabuzaid:123456789@ds111390.mlab.com:11390/doctor')
    .then(() => console.log('Database Connected'))
    .catch(() => console.log("Database Not Connected"));

app.use(bodyBarser.json());
app.use(bodyBarser.urlencoded({ extended: false }));
app.use(helmet());
app.use(morgan('dev'));
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, Authorization, X-Requested-With, Content-Type, Accept");
    next();
});
app.use('/api/appointment', require('./src/router/appointment'));
app.use('/api/prescription', require('./src/router/prescription'));
app.use('/api/patient', require('./src/router/patient'));
app.use('/api/medical', require('./src/router/patient-info'));
app.use('/api/doctors', require('./src/router/doctors'));
app.use('/api/schedule', require('./src/router/schedule'));
app.use('/api/portal', require('./src/router/users'));
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});
app.use((error, req, res, next) => {
    res.status(error.status || 500).json({
        message: error.message,
        data: null,
        status: error.status
    });
});
module.exports = app;
