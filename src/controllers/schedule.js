const model = require('../models/schedule');
const { SendErrors, response } = require('../helpers/handle-response');
require('../models/doctors');

module.exports = {
    Create: (req, res) => {
        const { id } = req.body;
        const New = new model({ chamber: id });
        New
            .save()
            .then(doc => res.status(201).json({ message: 'Done | Schedule Init', data: doc, status: 201 }))
            .catch(e => SendErrors(res, e));

    },
    Update: (req, res) => {
        const { id } = req.params;
        const { period } = req.body;
        model
            .findOneAndUpdate({ chamber: id }, { period })
            .then(doc => res.status(201).json({ message: 'Done | Schedule Updated', data: doc, status: 201 }))
            .catch(e => SendErrors(res, e));
    },
    Individual: (req, res) => {
        const { id } = req.params;
        model
            .findOne({ doctor: id })
            .populate('Doctors')
            .then(doc => res.status(201).json({ message: 'Done | Individual Schedule', data: doc, status: 201 }))
            .catch(e => SendErrors(res, e));
    }
}