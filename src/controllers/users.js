const model = require('../models/users');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const {
    SendErrors,
    response
} = require('../helpers/handle-response');

const token = user => {
    return jwt.sign({
        iss: 'doctors',
        sub: user.id
    }, 'secret', {
        expiresIn: '1h'
    });
}

module.exports = {
    SignUp: async (req, res) => {
        try {
            const {
                username,
                password,
                email
            } = req.body;
            const user = await model.findOne({
                username
            });
            if (user) return res.status(403).json({
                message: 'Faild | Doctor Already Exist',
                data: null,
                status: 403
            });
            console.log(req);
            const create = new model({
                username,
                password,
                email
            });
            await create.save();
            return res.status(201).json({
                message: 'Done | Doctor Created',
                data: true,
                status: 201
            })
        } catch (err) {
            SendErrors(err);
        }
    },
    SignIn: (req, res) => res.status(201).json({
        message: 'Done | Doctor Is In',
        data: {
            _id: req.user.id,
            token: token(req.user),
            username: req.user.username
        },
        status: 201
    })
}