'use strict';
const emitter = require('events').EventEmitter;
const em = new emitter();
const {
    SendErrors
} = require('../helpers/handle-response');
const model = require('../models/patient');

module.exports = {
    List: (req, res) => {
        let {
            limit,
            offset
        } = req.query;
        if (!limit) limit = 0;
        if (!offset) offset = 0;
        model
            .find({})
            .skip(Number(offset))
            .limit(Number(limit))
            .then((doc) => res.status(201).json({
                message: 'Done | Patient List',
                data: doc,
                status: 201
            }))
            .catch((e) => SendErrors(res, e));
    },
    Individual: (req, res) => {
        let {
            id
        } = req.params;
        model
            .findById(id)
            .then((doc) => res.status(201).json({
                message: 'Done | Selected Patient',
                data: doc,
                status: 201
            }))
            .catch((e) => SendErrors(res, e));
    },
    Create: (req, res) => {
        const {
            name,
            phone,
            address,
            age,
            gender
        } = req.body;
        const patient = new model({
            name,
            phone,
            address,
            age,
            gender
        });
        patient
            .save()
            .then((doc) => {
                res.status(201).json({
                    message: 'Done | Patient Added',
                    data: doc,
                    status: 201
                });
                em.emit('patientadded', doc._Id);
            })
            .catch((e) => SendErrors(res, e));
    },
    UpdateIndividual: (req, res) => {
        const {
            id
        } = req.params, {
            name,
            phone,
            address,
            age,
            gender
        } = req.body;
        const query = address ? {
            name,
            phone,
            address,
            age,
            gender
        } : {
            name,
            phone,
            age,
            gender
        };
        model
            .findByIdAndUpdate(id, query, {
                new: true
            })
            .then((doc) => res.status(201).json({
                message: 'Done | Patient Edited',
                data: doc,
                status: 201
            }))
            .catch((e) => SendErrors(res, e));
    },
    RemoveIndividual: (req, res) => {
        const {
            id
        } = req.params;
        const {
            name,
            phone,
            age,
            gender
        } = req.body;
        model
            .findByIdAndRemove(id)
            .then((doc) => res.status(201).json({
                message: 'Done | Patient Deleted',
                data: doc,
                status: 201
            }))
            .catch((e) => SendErrors(res, e));
    },
    RemoveAll: (req, res) => {
        model
            .remove({})
            .then((doc) => res.status(201).json({
                message: 'Done | Patient List Removed',
                data: doc,
                status: 201
            }))
            .catch((e) => SendErrors(res, e));
    }
}