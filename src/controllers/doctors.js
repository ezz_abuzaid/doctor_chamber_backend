const model = require('../models/doctors');
const { SendErrors } = require('../helpers/handle-response');

module.exports = {
    Create: (req, res, next) => {
        console.log(req.body);
        const { firstname, lastname, country, city, brithday, specialist, qualifications, department, address, gender, phone } = req.body;
        const New = new model({ firstname, lastname, country, city, brithday, specialist, qualifications, department, address, gender, phone });
        New
            .save()
            .then((doc) => res.status(201).json({ message: 'Done | Doctor Added', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    List: (req, res) => {
        model
            .find({})
            .then((doc) => res.status(201).json({ message: 'Done | Doctor List', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    IndividualList: (req, res) => {
        const { id } = req.params;
        model
            .findById(id)
            .then((doc) => res.status(201).json({ message: 'Done | Selected Doctor', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    UpdateIndividual: (req, res) => {
        const { id } = req.params;
        const { name, address } = req.body;
        model
            .findByIdAndUpdate(id, { name, address }, { new: true })
            .exec()
            .then((doc) => res.status(201).json({ message: 'Done | Doctor Updated', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    RemoveIndividual: (req, res) => {
        const { id } = req.params;
        model
            .findByIdAndRemove(id)
            .exec()
            .then((doc) => res.status(201).json({ message: 'Done | Doctor Removed', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    }
}