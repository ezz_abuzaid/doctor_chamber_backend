const model = require('../models/appointment');
module.exports = {
    List: (req, res, next) => {
        // model.aggregate([{
        //     $lookup:{
        //         from:'patient',
        //         localField:''
        //     }
        // }])
        const { date, patient } = req.query;
        if (date) var Find = model.find({ date });
        else if (patient) var Find = model.find({ patient });
        else return next();
        Find
            .populate('patient')
            .then((doc) => res.status(201).json({ message: 'Done | Apointment List', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    Create: (req, res) => {
        const { date, visited, patient } = req.body;
        const create = new model({ date, visited, patient });
        create
            .save()
            .then((doc) => res.status(201).json({ message: 'Done | Apointment Added', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    UpdateIndividual: (req, res) => {
        const { visited } = req.body;
        const { id } = req.params;
        model
            .findByIdAndUpdate(id, { visited }, { new: true })
            .then((doc) => res.status(201).json({ message: 'Done | Apointment Edited', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    Remove: (req, res) => {
        model
            .remove({})
            .then((doc) => res.status(201).json({ message: 'Done | Apointment Edited', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    }
}