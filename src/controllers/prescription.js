const model = require('../models/prescription');
const { SendErrors, response } = require('../helpers/handle-response');
const m = require('mongoose');
// Patient Schema to 
require('../models/patient');
module.exports = {
    List: (req, res) => {
        const { patient } = req.query;
        let Find;
        if (patient) Find = model.find({ patient });
        else Find = model.find({});
        Find
            .populate('patient', 'name')
            .then((doc) => res.status(201).json({ message: 'Done | Prescription List', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    IndividualList: (req, res) => {
        const { id } = req.params;
        model
            .findById(id)
            .populate('patient')
            .then((doc) => res.status(201).json({ message: 'Done | Selected Prescription', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    Create: (req, res) => {
        const { symptoms, diagnosis, medicine, tests, patient } = req.body;
        const New = new model({ symptoms, diagnosis, medicine, tests, patient });
        New.save()
            .then((doc) => res.status(201).json({ message: 'Done | Prescription Added', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    UpdateIndividual: (req, res) => {
        const { id } = req.params;
        const { symptoms, diagnosis, medicine, tests } = req.body;
        model.findByIdAndUpdate(id, {
            symptoms,
            diagnosis,
            medicine,
            tests
        }, { new: true })
            .then((doc) => res.status(201).json({ message: 'Done | Prescription Edited', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    Remove: (req, res) => {
        model.remove({})
            .then((doc) => res.status(201).json({ message: 'Done | Prescription Lists Removed', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    RemoveIndividual: (req, res) => {
        const { id } = req.params;
        model.findByIdAndRemove(id)
            .then((doc) => res.status(201).json({ message: 'Done | Prescription Lists Removed', data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    },
    RemoveSubInividual: (req, res) => {
        const { id, Lid, list } = req.params;
        let query;
        if (list === 'medicine') { query = { medicine: { _id: Lid } } }
        if (list === 'tests') { query = { tests: { _id: Lid } } }
        model.findByIdAndUpdate(id, { $pull: query }, { new: true })
            .then((doc) => res.status(201).json({ message: `Done | Prescription ${list} Deleted`, data: doc, status: 201 }))
            .catch((e) => SendErrors(res, e));
    }
}
