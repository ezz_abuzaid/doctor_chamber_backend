const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const sub = new Schema({
    name: String,
    note: String
});
const schema = new Schema({
    symptoms: { type: String },
    diagnosis: { type: String },
    medicine: [sub],
    tests: [sub],
    date: { type: Date, default: Date.now },
    patient: { type: Schema.Types.ObjectId, ref: 'Patient' }
});
module.exports = mongoose.model('Prescription', schema);