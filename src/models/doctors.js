const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const model = new Schema({
    firstname: {
        type: String,
        lowercase: true
    },
    lastname: {
        type: String,
        lowercase: true
    },
    country: {
        type: String,
        lowercase: true
    },
    city: {
        type: String,
        lowercase: true
    },
    brithday: {
        type: String,
        lowercase: true
    },
    specialist: {
        type: String,
        lowercase: true
    },
    qualifications: {
        type: String,
        lowercase: true
    },
    department: {
        type: String,
        lowercase: true
    },
    address: {
        type: String,
        lowercase: true
    },
    gender: {
        type: String,
        lowercase: true
    },
    phone: {
        type: String,
        lowercase: true
    }
});

module.exports = mongoose.model('Doctors', model);