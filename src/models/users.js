const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const schema = new Schema({
    username: { type: String, require: true, unique: true, lowercase: true },
    password: { type: String, require: true }, // Don't Forget To Add Password Confirmation
    email: { type: String, lowercase: true },
    phone: { type: String, lowercase: true }
    // ,
    // confirm: {
    //     type: String,
    //     lowercase: true
    // },
});
// For Register
schema.pre('save', async function(next) {
    try {
        if (!this.isModified('password')) return next();
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(this.password, salt);
        this.password = hash;
        next();
    } catch (error) {
        next(error);
    }
});
schema.methods.isValid = async function(password) {
    try {
        return await bcrypt.compare(password, this.password);
    } catch (error) {
        throw new Error(error);
    }
}
module.exports = mongoose.model('Users', schema);