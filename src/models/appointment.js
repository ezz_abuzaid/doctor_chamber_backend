const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    visited: {
        type: Boolean,
        default: false
    },
    date: {
        type: String,
        required: true
    },
    schedule: {
        type: String
    },
    patient: {
        type: Schema.Types.ObjectId,
        ref: 'Patient'
    }
});
module.exports = mongoose.model('Appointment', schema);