const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Intervals = new Schema({
    from: { type: String },
    to: { type: String }
});

const Shifts = new Schema({
    morning: Intervals,
    afternoon: Intervals,
    evening: Intervals,
    working: {
        type: Boolean,
        default: false
    }
});

const model = new Schema({
    period: {
        sunday: Shifts,
        monday: Shifts,
        tuesday: Shifts,
        wednesday: Shifts,
        thursday: Shifts,
        friday: Shifts,
        saturday: Shifts,
    },
    doctor: {
        type: Schema.Types.ObjectId,
        ref: 'Doctors'
    }
});

module.exports = mongoose.model('Schedule', model);