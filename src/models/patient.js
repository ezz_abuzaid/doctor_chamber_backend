const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Medical = {
    blood_group: { type: String, lowercase: true },
    height: { type: String, lowercase: true },
    weight: { type: String, lowercase: true },
    blood_pressure: { type: String, lowercase: true },
    pulse: { type: String, lowercase: true },
    respiration: { type: String, lowercase: true },
    allergy: { type: String, lowercase: true },
    diet: { type: String, lowercase: true },
    patient: { type: Schema.Types.ObjectId, ref: 'Patient' }
};

const Patient = new Schema({
    name: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    phone: {
        type: Number,
        required: true,
        maxlength: 10,
        minlength: 6
    },
    address: {
        type: String,
        lowercase: true,
        trim: true
    },
    age: { type: Number },
    gender: {
        type: String,
        enum: ['male', 'female']
    },
    // medical: { type: Schema.Types.ObjectId, ref: 'Medical' }
    // prescription: [{ type: Schema.Types.ObjectId, ref: 'Prescription' }],
    // appointment: [{ type: Schema.Types.ObjectId, ref: 'Appointment' }]
})
module.exports = mongoose.model('Patient', Patient);