'use strict';
const express = require('express');
const router = express.Router();
const { List, Individual, Create, UpdateIndividual, RemoveIndividual, RemoveAll } = require('../controllers/patient');
router.get('/list', List);
router.get('/list/:id', Individual);
router.post('/create', Create);
router.put('/update/:id', UpdateIndividual);
router.delete('/remove/:id', RemoveIndividual);
router.get('/remove', RemoveAll);

module.exports = router;