const express = require('express');
const router = require('express-promise-router')();
const passport = require('passport');
const { SignUp, SignIn } = require('../controllers/users');

require('../../passport');
const Log = passport.authenticate('local', { session: false });

router.post('/signup', SignUp);

router.post('/signin', Log, SignIn);

module.exports = router;