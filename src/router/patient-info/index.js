const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const emitter = require('events').EventEmitter;
const em = new emitter();
const {
    SendErrors,
    response
} = require('../../helpers/handle-response');

// Schema Import
const model = require('./schema');

router.get('/:id', (req, res) => {
    const {
        id
    } = req.params;
    model
        .findOne({
            patient: id
        })
        .then((doc) => res.status(201).json({
            message: 'Done | Patient Info',
            data: doc,
            status: 201
        }))
        .catch((e) => SendErrors(res, e));
});

router.put('/update/:id', (req, res) => {

    const {
        id
    } = req.params;

    const {
        blood_group,
        height,
        weight,
        blood_pressure,
        pulse,
        respiration,
        allergy,
        diet
    } = req.body;

    model.findByIdAndUpdate(id, {
            blood_group,
            height,
            weight,
            blood_pressure,
            pulse,
            respiration,
            allergy,
            diet
        }, {
            new: true
        })
        .then((doc) => res.status(201).json({
            message: 'Done | Patient Info Updated',
            data: doc,
            status: 201
        }))
        .catch((e) => SendErrors(res, e));
});

em.on('patientadded', (patient) => {
    const Medical = new model({
        patient
    });
    Medical
        .save()
        .then((doc) => res.status(201).json({
            message: 'Done | Patient Info Created',
            data: doc,
            status: 201
        }))
        .catch((e) => SendErrors(res, e));
})
// router.post('/create', (req, res) => {

//     const {
//         patient
//     } = req.body;

// });

module.exports = router;
