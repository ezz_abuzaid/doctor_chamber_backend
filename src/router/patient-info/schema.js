const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Medical = new Schema({
    blood_group: { type: String, lowercase: true },
    height: { type: String, lowercase: true },
    weight: { type: String, lowercase: true },
    blood_pressure: { type: String, lowercase: true },
    pulse: { type: String, lowercase: true },
    respiration: { type: String, lowercase: true },
    allergy: { type: String, lowercase: true },
    diet: { type: String, lowercase: true },
    patient: { type: Schema.Types.ObjectId, ref: 'Patient' }
});

module.exports = mongoose.model('Medical', Medical);;