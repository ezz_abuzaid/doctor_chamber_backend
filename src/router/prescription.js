const express = require('express');
const router = express.Router();
const { List, IndividualList, Create, UpdateIndividual, RemoveIndividual, RemoveSubInividual, Remove } = require('../controllers/prescription');
const passport = require('passport');
const Auth = passport.authenticate('jwt', { session: false });

router.get('/list', Auth, List);

router.get('/list/:id', IndividualList);

router.post('/create', Create);

router.put('/update/:id', UpdateIndividual);

router.delete('/remove/:id', RemoveIndividual);

router.delete('/remove/:id/:Lid/:list', RemoveSubInividual);

router.delete('/remove', Remove);

module.exports = router;