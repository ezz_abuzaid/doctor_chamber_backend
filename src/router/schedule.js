const express = require('express');
const router = express.Router();
const { Create, Update, Individual } = require('../controllers/schedule');

router.get('/list/:id', Individual);

router.post('/create', Create);

router.put('/update/:id', Update);

module.exports = router;