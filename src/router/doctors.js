const express = require('express');
const router = express.Router();
const { Create, List, IndividualList, UpdateIndividual, RemoveIndividual } = require('../controllers/doctors');

const multer = require('multer');
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/', function(err, a) { if (err) console.log(err) })
    },
    filename: function(req, file, cb) {
        cb(null, `${Date.now()}.${file.originalname}`)
    }
});
const upload = multer({
    storage,
    limits: {
        fieldSize: 1024 * 1024 * 5
    },
    fileFilter: function(req, file, cb) {
        if (!(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || !file)) cp(null, false);
        else cp(null, true);
    }
});
router.get('/list', List);

router.get('/list/:id', IndividualList);

router.post('/create', upload.single('image'), Create);

router.put('/update/:id', UpdateIndividual);

router.delete('/remove:id', RemoveIndividual);

module.exports = router;