const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const { SendErrors } = require('../helpers/handle-response');
const { List, Create, UpdateIndividual, Remove } = require('../controllers/appointment')

require('../../passport');
const passport = require('passport');
const Log = passport.authenticate('local', { session: false });

router.get('/list', List);

router.post('/add', Create);
router.put('/update/:id', UpdateIndividual);
router.get('/remove', Remove);
module.exports = router;