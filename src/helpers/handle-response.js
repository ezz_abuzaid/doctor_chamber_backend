const response = { status: 200, message: 'Send Successfully', data: [] };
const SendErrors = (res, err) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    response.data = null;
    res.status(501).json(response);
};

module.exports = {
    response,
    SendErrors
}