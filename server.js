const http = require('http');
const app = require('./app');
app.set('port', process.env.PORT || 8080);

http.createServer(app)
    .listen(app.get('port'), () => console.log('Backend Running'));