const passport = require('passport');
const jwtStrategy = require('passport-jwt').Strategy;
const localStrategy = require('passport-local').Strategy;
const { ExtractJwt } = require('passport-jwt');
const User = require('./src/models/users');

passport.use(new jwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: 'secret'
}, async(payload, done) => {
    try {
        const user = await User.findById(payload.sub);
        if (!user) return done(null, false);
        done(null, user)
    } catch (error) {
        done(error, false)
    }
}));

passport.use(new localStrategy({
    usernameField: 'username'
}, async(username, password, done) => {
    try {
        const user = await User.findOne({ username });
        if (!user) return done(null, false);
        const Is = await user.isValid(password);
        if (!Is) return done(null, false);
        done(null, user);
    } catch (error) {
        done(error, false)
    }
}));